class Sayonghelloworld::HelloController < ApplicationController

  def scratch

    if params.has_key?(:deleteAll)
      Hello.delete_all
    end

    if params.has_key?(:id)
      Hello.delete(params[:id].to_i)
    end

    @all_hello_items = Hello.all
  end

  def rub

    if params.has_key?(:description)
      description = params[:description]
    end
    if params.has_key?(:name)
      name = params[:name]
      name = name.to_s.capitalize
    end

    if params.has_key?(:hello)
      hello = params[:hello]
      description = hello[:description]
      name = hello[:name]
    end


    if params.has_key?(:id)
      id = params[:id].to_i
      if Hello.all.empty?
        Hello.create(name: name, description: description)
      else
        if id>= Hello.first.id.to_i && id <= Hello.last.id.to_i
          Hello.find(id).update(name: name, description: description)
        else
          Hello.create(name: name, description: description)
        end
      end
    end

    @record = Hello.last
    if params.has_key?(:id)
      @record = Hello.find(params[:id].to_i)
    end
  end

  def edit
    @record = Hello.last
    if params.has_key?(:id)
      @record = Hello.find(params[:id].to_i)
    end
  end

  def create
    @record = Hello.new
  end

  def created
    if params.has_key?(:hello)
      hello = params[:hello]
      description = hello[:description]
      name = hello[:name]
      created = Hello.create(description: description, name: name)
      @message = created.errors.messages
      if @message.empty?
        @message = 'Welcome in my Twitter :D!'
      end
    end
  end
end
